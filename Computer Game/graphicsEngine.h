#pragma once
class graphicsEngine
{
	//Klasa odpowiedzialna za generowanie i wy�wietlanie grafik
	//========================================================

	//Obiekty wy�wietlane
	sf::RenderWindow& window;
	std::vector< std::vector <Pixel> >& map;
	sf::RectangleShape bcg;
	sf::RectangleShape middle;
	sf::RectangleShape small;
	sf::RectangleShape bars;
	sf::RectangleShape barsVertical;
	sf::RectangleShape exitButton;
	sf::RectangleShape acceptButton;
	sf::RectangleShape endPannel;
	sf::Font font;
	sf::Text message;
	sf::Texture texture[19];
	sf::Sprite sprites[19];
	sf::Sprite tempSprite;

	//Funkcje wspomagaj�ce generowanie grafik
	void createCenteredText(int x0, int width, int y0, int height, std::string msg, bool smallText, sf::Color color);
	void createBars(int y, float min, float max, float active, std::string info);
	void graphicsLoad();
	void graphicsObjectsCreator();
	void commonRotation(int x, int y, sf::Sprite& tempSprite);

	//D�ugo�� boku kratki
	int poolCorner;

public:
	graphicsEngine(sf::RenderWindow& w, std::vector< std::vector <Pixel> >& m);

	//Generowanie odpowiednich scen
	void generateMain();
	void generatePre(float width, float height, float PCs, int PCSmin, int PCSmax);
	void info();
	void prepareToGame(int width, int height);
	void game();
	void end();
	int getPoolCorner()
	{
		return poolCorner;
	}
};


graphicsEngine::graphicsEngine(sf::RenderWindow& w, std::vector< std::vector <Pixel> >& m) : window(w), map(m)
{
	if (!font.loadFromFile("arial.ttf"))
		abort();
	message.setFont(font);
	message.setCharacterSize(window.getSize().y / 16);
	message.setFillColor(sf::Color(63, 72, 204));

	graphicsLoad();
	graphicsObjectsCreator();

	poolCorner = 64;
}

void graphicsEngine::graphicsObjectsCreator()
{
	//Ustawianie parametr�w obiekt�w wy�wietlanych

	bcg.setSize(sf::Vector2f(window.getSize().x, window.getSize().y));
	bcg.setFillColor(sf::Color(171, 171, 194));

	middle.setSize(sf::Vector2f(window.getSize().x / 2, window.getSize().y * 3 / 4));
	middle.setPosition(sf::Vector2f(window.getSize().x / 4, window.getSize().y / 10));
	middle.setFillColor(sf::Color(171, 194, 171));

	small.setSize(sf::Vector2f(window.getSize().x / 3, window.getSize().y / 6));
	small.setFillColor(sf::Color(194, 171, 171));

	bars.setSize(sf::Vector2f(window.getSize().x * 4 / 9, window.getSize().y / 64));
	bars.setFillColor(sf::Color(0, 0, 0));
	bars.setPosition(sf::Vector2f(window.getSize().x*0.278, 0));

	barsVertical.setSize(sf::Vector2f(window.getSize().x / 64, window.getSize().y / 32));
	barsVertical.setFillColor(sf::Color(0, 0, 0));
	barsVertical.setPosition(sf::Vector2f(window.getSize().x*0.278, 0));

	exitButton.setSize(sf::Vector2f(window.getSize().x / 4, window.getSize().y / 16));
	exitButton.setFillColor(sf::Color(240, 0, 0));
	exitButton.setPosition(sf::Vector2f(0, window.getSize().y * 30 / 32));

	acceptButton.setSize(sf::Vector2f(window.getSize().x / 4, window.getSize().y / 16));
	acceptButton.setFillColor(sf::Color(84, 160, 197));
	acceptButton.setPosition(sf::Vector2f(window.getSize().x * 3 / 4, window.getSize().y * 30 / 32));

	endPannel.setSize(sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));
	endPannel.setFillColor(sf::Color(8, 170, 16));
	endPannel.setPosition(sf::Vector2f(window.getSize().x / 4, window.getSize().y / 4));
	endPannel.setOutlineColor(sf::Color(36, 62, 39));
	endPannel.setOutlineThickness(5);
}

void graphicsEngine::graphicsLoad()
{
	//�adowanie grafik
	if (!texture[0].loadFromFile("graphics/C0.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[1].loadFromFile("graphics/C01.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[2].loadFromFile("graphics/C10.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[3].loadFromFile("graphics/C1.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[4].loadFromFile("graphics/E0.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[5].loadFromFile("graphics/E1.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[6].loadFromFile("graphics/H0.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[7].loadFromFile("graphics/H01.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[8].loadFromFile("graphics/H1.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[9].loadFromFile("graphics/I0.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[10].loadFromFile("graphics/I1.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[11].loadFromFile("graphics/L0.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[12].loadFromFile("graphics/L1.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[13].loadFromFile("graphics/X0.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[14].loadFromFile("graphics/X1.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[15].loadFromFile("graphics/P0.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[16].loadFromFile("graphics/P1.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[17].loadFromFile("graphics/Q.png", sf::IntRect(0, 0, 64, 64)))
		abort();
	if (!texture[18].loadFromFile("graphics/H10.png", sf::IntRect(0, 0, 64, 64)))
		abort();

	for (int i = 0; i < 19; i++) sprites[i].setTexture(texture[i]);
}

void graphicsEngine::createCenteredText(int x0, int width, int y0, int height, std::string msg, bool smallText, sf::Color color)
{
	//Funkcja matematyczna, w argumencie otrzymuje:
	//x, y lewego g�rnego rogu (obiektu odniesienia).
	//wysoko��, szeroko�� obiektu odniesienia
	//wiadomo�� do wy�wietlenia
	//warto�� bool wskazuj�c�, czy wy�wietlany tekst jest du�� czcionk�, czy ma��
	//kolor tekstu

	if (smallText) message.setCharacterSize(window.getSize().y / 32);
	else message.setCharacterSize(window.getSize().y / 16);
	message.setFillColor(color);
	message.setString(msg);
	float w = message.getGlobalBounds().width, h = message.getGlobalBounds().height;
	float x = (width / 2 - w / 2) + x0;
	float y = (height / 2 - h / 2) + y0;
	message.setPosition(sf::Vector2f(x, y));
	window.draw(message);
}

void graphicsEngine::createBars(int y, float min, float max, float active, std::string info)
{
	//Funkcja tworz�ca 'suwaki; wyboru parametr�w mapy
	//W argumencie dostaje:
	//Wysoko�� suwaka (po�o�enie na planszy)
	//Minimaln� wy�wietlan� warto��
	//Maksymaln� wy�wietlan� warto��
	//Bie��c� wy�wietlan� warto��
	//Nazw� suwaka

	message.setCharacterSize(window.getSize().y / 32);
	createCenteredText(window.getSize().x / 2, 0, y - 4 * message.getCharacterSize(), message.getCharacterSize() / 4, info, true, sf::Color(0, 0, 0));
	bars.setPosition(sf::Vector2f(window.getSize().x*0.278, y));
	window.draw(bars);

	createCenteredText(window.getSize().x*0.278, 0, y - window.getSize().y / 16, 0, std::to_string(min).substr(0, 2), true, sf::Color(0, 0, 0));
	barsVertical.setPosition(sf::Vector2f(window.getSize().x*0.278, y - (window.getSize().y / 16) / 4));
	window.draw(barsVertical);

	createCenteredText(window.getSize().x*0.278 + window.getSize().x * 4 / 9 - window.getSize().x / 64, 0, y - window.getSize().y / 16, 0, std::to_string(max).substr(0, 3), true, sf::Color(0, 0, 0));
	barsVertical.setPosition(sf::Vector2f(window.getSize().x*0.278 + window.getSize().x * 4 / 9 - window.getSize().x / 64, y - (window.getSize().y / 16) / 4));
	window.draw(barsVertical);

	int x = int(float(((active - min) / (max - min))*float(window.getSize().x * 0.429)));
	x += window.getSize().x * 0.278;

	barsVertical.setPosition(sf::Vector2f(x, y - window.getSize().y / 32));
	window.draw(barsVertical);
	message.setOutlineColor(sf::Color(255, 255, 255));
	message.setOutlineThickness(2);
	createCenteredText(x, 0, y - window.getSize().y / 16, 0, std::to_string(active).substr(0, 3), true, sf::Color(164, 37, 19));
	message.setOutlineThickness(0);
}

void graphicsEngine::generateMain()
{
	//Funkcja generuj�ca Menu g��wne
	window.draw(bcg);

	window.draw(middle);


	small.setPosition(sf::Vector2f(window.getSize().x / 3, window.getSize().y / 4 - window.getSize().y / 10));
	window.draw(small);
	createCenteredText(window.getSize().x / 3, window.getSize().x / 3, (window.getSize().y / 4 - window.getSize().y / 10) - 10,
		window.getSize().y / 6, "Nowa Gra", false, sf::Color(63, 72, 204));

	small.setPosition(sf::Vector2f(window.getSize().x / 3, window.getSize().y / 2 - window.getSize().y / 10));
	window.draw(small);
	createCenteredText(window.getSize().x / 3, window.getSize().x / 3, (window.getSize().y / 2 - window.getSize().y / 10) - 10,
		window.getSize().y / 6, "Informacje", false, sf::Color(63, 72, 204));

	small.setPosition(sf::Vector2f(window.getSize().x / 3, (window.getSize().y / 4) * 3 - window.getSize().y / 10));
	window.draw(small);
	createCenteredText(window.getSize().x / 3, window.getSize().x / 3, ((window.getSize().y / 4) * 3 - window.getSize().y / 10) - 10,
		window.getSize().y / 6, "EXIT", false, sf::Color(63, 72, 204));
}

void graphicsEngine::generatePre(float width, float height, float PCs, int PCSmin, int PCSmax)
{
	//Generowanie ekranu wyboru parametr�w gry

	window.draw(bcg);
	window.draw(middle);
	createBars(window.getSize().y * 0.25, 10, 100, width, "Szerokosc pola gry");
	createBars(window.getSize().y * 0.5, 10, 100, height, "Wysokosc pola gry");
	createBars(window.getSize().y * 0.75, PCSmin, PCSmax, PCs, "Ilosc komputerow");

	window.draw(exitButton);
	window.draw(acceptButton);

	createCenteredText(0, window.getSize().x / 4, window.getSize().y * 30 / 32, window.getSize().y / 32,
		"Powrot", false, sf::Color(255, 255, 255));

	createCenteredText(window.getSize().x * 3 / 4, window.getSize().x / 4, window.getSize().y * 30 / 32, window.getSize().y / 32,
		"Start", false, sf::Color(255, 255, 255));
}

void graphicsEngine::info()
{
	//Generowanie i wy�wietlanie ekranu informacyjnego

	window.draw(bcg);
	window.draw(exitButton);
	createCenteredText(0, window.getSize().x / 4, window.getSize().y * 30 / 32, window.getSize().y / 32,
		"Powrot", false, sf::Color(255, 255, 255));

	createCenteredText(window.getSize().x/2, 0, window.getSize().y * 3 / 32, 0,
		"Witaj w grze 'Connect PCs!'", false, sf::Color(0, 0, 0));

	createCenteredText(window.getSize().x / 2, 0, window.getSize().y * 6 / 32, 0,
		"Twoim zadaniem jest polaczyc komputery z zasilaniem", true, sf::Color(0, 0, 0));

	createCenteredText(window.getSize().x / 2, 0, window.getSize().y * 8 / 32, 0,
		"Kliknij w klocek aby go obrocic o 90 stopni", true, sf::Color(0, 0, 0));

	createCenteredText(window.getSize().x / 2, 0, window.getSize().y * 10 / 32, 0,
		"Zycze milej zabawy!", true, sf::Color(0, 0, 0));

	createCenteredText(window.getSize().x / 2, 0, window.getSize().y * 29 / 32, 0,
		"Made by Jakub Olszewski", true, sf::Color(0, 2, 74));
}

void graphicsEngine::prepareToGame(int width, int height)//Funkcja mojego autorstwa, zaadoptowana z projektu SNAKE
{
	//Funkcja ustalaj�ca wymiary blok�w, aby mapa zawsze mie�ci�a si� w oknie gry
	//G��wnie matematyka

	float poolCorner = 64;
	float XPXO = float(poolCorner * width) / float(window.getSize().x);
	float YPYO = float(poolCorner * height) / float(window.getSize().y);


	if (XPXO < 1 && YPYO < 1)
	{
		if (XPXO < YPYO) poolCorner *= (1 / YPYO);
		else poolCorner *= (1 / XPXO);
	}
	else if (XPXO >= 1 && YPYO < 1) poolCorner *= (1 / XPXO);
	else if (XPXO < 1 && YPYO >= 1) poolCorner *= (1 / YPYO);
	else if (XPXO >= 1 && YPYO >= 1)
	{
		if (XPXO < YPYO) poolCorner *= (1 / YPYO);
		else poolCorner *= (1 / XPXO);
	}
	graphicsEngine::poolCorner = floor(poolCorner);
}

void graphicsEngine::commonRotation(int x, int y, sf::Sprite& tempSprite)
{
	//Funkcja obracaj�ca bloki o dan� ilo�� stopni
	//x - koordynat x
	//y - koordynat y
	//tempSprite - wska�nik na grafik�

	if (map[y][x].rot == RIGHT)
	{
		tempSprite.setRotation(90);
		tempSprite.setPosition(sf::Vector2f(poolCorner*(x + 1), poolCorner*(y)));
	}
	if (map[y][x].rot == DOWN)
	{
		tempSprite.setRotation(180);
		tempSprite.setPosition(sf::Vector2f(poolCorner*(x + 1), poolCorner*(y + 1)));
	}
	if (map[y][x].rot == LEFT)
	{
		tempSprite.setRotation(270);
		tempSprite.setPosition(sf::Vector2f(poolCorner*(x), poolCorner*(y + 1)));
	}
}

void graphicsEngine::game()
{
	//Funkcja rysuj�ca sam� gr� (rozgrywk�)

	window.draw(bcg);

	for (int y = 0; y < map.size(); y++)
	{
		for (int x = 0; x < map[0].size(); x++)
		{

			if (map[y][x].isPC)
			{
				if (map[y][x].isActive0) tempSprite = sprites[16];
				else tempSprite = sprites[15];

				tempSprite.scale(float(poolCorner) / 64, float(poolCorner) / 64);
				tempSprite.setPosition(sf::Vector2f(poolCorner*x, poolCorner*y));
				window.draw(tempSprite);
			}

			else if (map[y][x].type == I)
			{
				if (map[y][x].isActive0)
					tempSprite = sprites[10];
				else tempSprite = sprites[9];

				tempSprite.scale(float(poolCorner) / 64, float(poolCorner) / 64);
				tempSprite.setPosition(sf::Vector2f(poolCorner*x, poolCorner*y));

				if (map[y][x].rot == LEFT || map[y][x].rot == RIGHT)
				{
					tempSprite.setRotation(90);
					tempSprite.setPosition(sf::Vector2f(poolCorner*(x + 1), poolCorner*(y)));
				}
				window.draw(tempSprite);
			}

			else if (map[y][x].type == L)
			{
				if (map[y][x].isActive0)
					tempSprite = sprites[12];
				else tempSprite = sprites[11];

				tempSprite.scale(float(poolCorner) / 64, float(poolCorner) / 64);
				tempSprite.setPosition(sf::Vector2f(poolCorner*x, poolCorner*y));
				commonRotation(x, y, tempSprite);
				window.draw(tempSprite);
			}

			else if (map[y][x].type == E)
			{
				if (map[y][x].isActive0)
					tempSprite = sprites[5];
				else tempSprite = sprites[4];

				tempSprite.scale(float(poolCorner) / 64, float(poolCorner) / 64);
				tempSprite.setPosition(sf::Vector2f(poolCorner*x, poolCorner*y));
				commonRotation(x, y, tempSprite);
				window.draw(tempSprite);
			}

			else if (map[y][x].type == X)
			{
				if (map[y][x].isActive0)
					tempSprite = sprites[14];
				else tempSprite = sprites[13];

				tempSprite.scale(float(poolCorner) / 64, float(poolCorner) / 64);
				tempSprite.setPosition(sf::Vector2f(poolCorner*x, poolCorner*y));
				window.draw(tempSprite);
			}

			else if (map[y][x].type == H)
			{
				if (map[y][x].isActive0 && !map[y][x].isActive1)
					tempSprite = sprites[7];
				else if (!map[y][x].isActive0 && map[y][x].isActive1)
					tempSprite = sprites[18];
				else if (map[y][x].isActive0 && map[y][x].isActive1)
					tempSprite = sprites[8];
				else tempSprite = sprites[6];

				if (map[y][x].rot == RIGHT || map[y][x].rot == LEFT)
				{
					if ((map[y][x].isActive0 && !map[y][x].isActive0) || (!map[y][x].isActive0 && map[y][x].isActive0))
					{
						bool pom = map[y][x].isActive0;
						map[y][x].isActive0 = map[y][x].isActive1;
						map[y][x].isActive1 = pom;
					}
				}

				tempSprite.scale(float(poolCorner) / 64, float(poolCorner) / 64);
				tempSprite.setPosition(sf::Vector2f(poolCorner*x, poolCorner*y));


				if (map[y][x].rot == RIGHT || map[y][x].rot == LEFT)
				{
					//tempSprite.setRotation(180);
					//tempSprite.setPosition(sf::Vector2f(poolCorner*(x + 1), poolCorner*(y + 1)));
					tempSprite.scale(-1.f, 1.f);
					tempSprite.setPosition(sf::Vector2f(poolCorner*(x + 1), poolCorner*(y)));
				}

				window.draw(tempSprite);
			}

			else if (map[y][x].type == C)
			{
				if (map[y][x].isActive0 && !map[y][x].isActive1)
					tempSprite = sprites[2];
				else if (!map[y][x].isActive0 && map[y][x].isActive1)
					tempSprite = sprites[1];
				else if (map[y][x].isActive0 && map[y][x].isActive1)
					tempSprite = sprites[3];
				else tempSprite = sprites[0];

				tempSprite.scale(float(poolCorner) / 64, float(poolCorner) / 64);
				tempSprite.setPosition(sf::Vector2f(poolCorner*x, poolCorner*y));
				window.draw(tempSprite);
			}

			if (map[y][x].isPSU)
			{
				tempSprite = sprites[17];
				tempSprite.scale(float(poolCorner) / 64, float(poolCorner) / 64);
				tempSprite.setPosition(sf::Vector2f(poolCorner*x, poolCorner*y));
				window.draw(tempSprite);
			}
		}
	}
}

void graphicsEngine::end()
{
	//Wy�wietlanie panelu ko�cowego z informacj� o wygranej

	game();
	window.draw(endPannel);
	createCenteredText(window.getSize().x / 4, window.getSize().x / 2, window.getSize().y / 4, window.getSize().y / 8,
		"Wygrana!", false, sf::Color(221, 193, 0));

	createCenteredText(window.getSize().x / 4, window.getSize().x / 2, window.getSize().y / 2, window.getSize().y / 8,
		"Kliknij przycisk myszy, aby kontynuowac", true, sf::Color(221, 193, 0));
}