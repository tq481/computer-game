#pragma once

class inputHandler
{
	//Klasa zarz�dzaj�ca odbieraniem informacji o ruchach gracza,
	//wy�wietlaj�ca bie��cy stan gry, panele informacyjne
	//ogarniaj�ca ca�� rozgrywk�

	sf::RenderWindow& window;
	graphicsEngine& engine;
	BoardController& controller;
	sf::Event localEvent;

	void active(); //Wybieranie i zlecanie rysowania odpowieniego panelu gry
	bool isActivated(int x0, int y0, int x1, int y1);
	whereIAm presentScreen; //Bie��cy ekran
	PreGameData dataToPass; //Dane do rozpocz�cia rozgrywki

public:
	inputHandler(sf::RenderWindow& w, graphicsEngine& g, BoardController& c);

	bool switcher();//G��wna p�tla
	void passEvent(sf::Event& event1);
};

inputHandler::inputHandler(sf::RenderWindow& w, graphicsEngine& g, BoardController& c) : window(w), engine(g), controller(c)
{
	presentScreen = MAIN;
	dataToPass.height = 10;
	dataToPass.width = 10;
	dataToPass.PCs = 10;
}

void inputHandler::passEvent(sf::Event& event1)
{
	if (event1.type == sf::Event::MouseButtonPressed)
	{
		localEvent = event1;
	}
}

bool inputHandler::isActivated(int x0, int x1, int y0, int y1)
{
	if (localEvent.mouseButton.x >= x0 && localEvent.mouseButton.x <= x1)
	{
		if (localEvent.mouseButton.y >= y0 && localEvent.mouseButton.y <= y1)
		{
			localEvent = sf::Event();
			return true;
		}
	}
	return false;
}

bool inputHandler::switcher()
{
	if (presentScreen == MAIN)
	{
		if (isActivated(window.getSize().x / 3, (window.getSize().x / 3) * 2, (window.getSize().y / 4) * 3 - window.getSize().y / 10,
			(((window.getSize().y / 4) * 3 - window.getSize().y / 10) + (window.getSize().y / 6))))
		{
			return false;
		}

		if (isActivated(window.getSize().x / 3, (window.getSize().x / 3) * 2,
			(window.getSize().y / 4 - window.getSize().y / 10), ((window.getSize().y / 4 - window.getSize().y / 10) + (window.getSize().y / 6))))
		{
			presentScreen = PRE;
			return true;
		}

		if (isActivated(window.getSize().x / 3, (window.getSize().x / 3) * 2,
			window.getSize().y / 2 - window.getSize().y / 10, (window.getSize().y / 2 - window.getSize().y / 10) + (window.getSize().y / 6)))
		{
			presentScreen = INFO;
			return true;
		}
	}

	if (presentScreen == INFO)
	{
		if (isActivated(0, window.getSize().x / 4, window.getSize().y * 15 / 16, window.getSize().y))
		{
			presentScreen = MAIN;
			return true;
		}
	}

	if (presentScreen == PRE)
	{
		if (isActivated(0, window.getSize().x / 4, window.getSize().y * 15 / 16, window.getSize().y))
		{

			presentScreen = MAIN;
			return true;
		}

		if (isActivated(window.getSize().x * 3 / 4, window.getSize().x, window.getSize().y * 15 / 16, window.getSize().y))
		{
			controller.initiateBoard(dataToPass.width, dataToPass.height, dataToPass.PCs);
			engine.prepareToGame(dataToPass.width, dataToPass.height);
			controller.mapUpdater(0, 0, 0);
			presentScreen = GAME;
			return true;
		}


		if (localEvent.mouseButton.x >= window.getSize().x*0.278 && localEvent.mouseButton.x <= window.getSize().x*0.278 + window.getSize().x * 4 / 9)
		{
			//Szeroko��
			if (localEvent.mouseButton.y >= window.getSize().y * 0.24 && localEvent.mouseButton.y <= window.getSize().y * 0.26 + window.getSize().y / 64)
			{
				int val = (localEvent.mouseButton.x - window.getSize().x * 0.278) * (90) / (window.getSize().x * 0.44) + 10;
				dataToPass.width = val;
				if (dataToPass.PCs > sqrt(dataToPass.width * dataToPass.height)) dataToPass.PCs = sqrt(dataToPass.width * dataToPass.height);
			}

			//Wysoko��
			if (localEvent.mouseButton.y >= window.getSize().y * 0.49 && localEvent.mouseButton.y <= window.getSize().y * 0.51 + window.getSize().y / 64)
			{
				int val = (localEvent.mouseButton.x - window.getSize().x * 0.278) * (90) / (window.getSize().x * 0.44) + 10;
				dataToPass.height = val;
				if (dataToPass.PCs > sqrt(dataToPass.width * dataToPass.height)) dataToPass.PCs = sqrt(dataToPass.width * dataToPass.height);
			}

			//Komputery
			if (localEvent.mouseButton.y >= window.getSize().y * 0.74 && localEvent.mouseButton.y <= window.getSize().y * 0.76 + window.getSize().y / 64)
			{
				int val = (localEvent.mouseButton.x - window.getSize().x * 0.278) * (sqrt(dataToPass.width*dataToPass.height) * 0.9) / (window.getSize().x * 0.44) + sqrt(dataToPass.width*dataToPass.height) / 10;
				if (val > sqrt(dataToPass.width * dataToPass.height)) val = sqrt(dataToPass.width * dataToPass.height);
				dataToPass.PCs = val;
			}
		}
	}

	if (presentScreen == GAME)
	{
		int xpos = localEvent.mouseButton.x / engine.getPoolCorner();
		int ypos = localEvent.mouseButton.y / engine.getPoolCorner();

		if (controller.mapUpdater(xpos, ypos, localEvent.mouseButton.button == sf::Mouse::Button::Left))
			presentScreen = END;
		localEvent.mouseButton.x = window.getSize().x + 1;
	}

	if (presentScreen == END)
	{
		int xpos = localEvent.mouseButton.x;
		int ypos = localEvent.mouseButton.y;

		if (localEvent.mouseButton.x >= 0 && localEvent.mouseButton.x <= window.getSize().x)
			presentScreen = MAIN;

		localEvent.mouseButton.x = window.getSize().x + 1;

	}

	active();

	return true;//Je�eli false, oznacza to, �e gra zosta�a zako�czona
}

void inputHandler::active()
{
	if (presentScreen == MAIN)
		engine.generateMain();

	if (presentScreen == PRE)
		engine.generatePre(dataToPass.width, dataToPass.height, dataToPass.PCs, sqrt(dataToPass.width*dataToPass.height) / 10, sqrt(dataToPass.width*dataToPass.height));

	if (presentScreen == INFO)
		engine.info();

	if (presentScreen == GAME)
	{
		controller.connectionChecker();
		engine.game();
	}

	if (presentScreen == END)
		engine.end();

}