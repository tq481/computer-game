#pragma once

class BoardGenerator
{
	//Generator mapy

	std::vector <std::vector <Pixel> > & map;
	std::vector <std::vector <bool> > canBePlaced;
	std::vector <coordinateData> PCs;
	std::vector <coordinateData> generatedPaths;
	coordinateData PSU;

	//Poj�cie [Wirtualne przeszkody] odnosi si� do przeszk�d widzianych
	//Wy��cznie przez funkcje programu, nie maj� znaczenia
	//i nie s� widoczne dla gracza

	//Funkcje s� opisane wewn�trz funkcji
	void recreateBoolMap();
	void placePCs(int PCAmount);
	void obstacleCreator();
	void onListAdder(std::vector <rewindData>& rewindVector, int x, int y, int p);
	void mapCreator();
	void nullFiller();
	int PCCoordinateID(std::vector <rewindData>& rewindVector, int x, int y);
	bool isPCConflict(coordinateData& pc) const;
	bool isInHigherBlockChanceZone(int x, int y) const;
	bool coordinateCorrectChecher(int x, int y) const;
	bool hasBeenActivated(std::vector <rewindData>& rewindVector, int x, int y) const;
	bool hasBeenPlaced(std::vector <coordinateData>& rewindVector, int x, int y) const;
	bool pathFinder();
	bool arePCsOnList(std::vector <rewindData>& rewindVector) const;

	bool debugMode;
public:
	BoardGenerator(std::vector <std::vector <Pixel> > & m);
	void generateMap(int PCAmount);
	void useDebugMode();
	void exitDebugMode();
};

BoardGenerator::BoardGenerator(std::vector <std::vector <Pixel> > & m) : map(m) 
{
	debugMode = false;
}

void BoardGenerator::recreateBoolMap()
{
	//Resetuje i czy�ci mapy boolianowe potrzebne do operacji logicznych

	canBePlaced.clear();
	bool pom = true;
	for (int y = 0; y < map.size(); y++)
	{
		std::vector <bool> pomVec;
		for (int x = 0; x < map[0].size(); x++)
		{
			pomVec.push_back(pom);
		}
		canBePlaced.push_back(pomVec);
	}
}

bool BoardGenerator::isPCConflict(coordinateData& pc) const
{
	for (int i = 0; i < PCs.size(); i++)
		if (sqrt(pow(PCs[i].x - pc.x,2) + pow(PCs[i].y - pc.y,2)) < map.size()/8) return true;
	return false;
}

void BoardGenerator::placePCs(int PCAmount)
{
	//Generuje (losuje) po�o�enia komputer�w na mapie
	while (PCs.size() < PCAmount)
	{
		coordinateData PC = PSU;

		while (true)
		{
			PC.x = rand() % (map[0].size());
			PC.y = rand() % (map.size());
			if(sqrt(pow((PSU.x - PC.x), 2) + pow((PSU.y - PC.y), 2)) > 2)
				if (!isPCConflict(PC)) break;
			//Je�eli komputeru s� za blisko siebie lub zasilacza
			//koordynaty losowane s� ponownie
		}

		PCs.push_back(PC);
	}

	if (debugMode)
	{
		for (int i = 0; i < PCs.size(); i++)
		{
			std::cout << "PC:  X: " << PCs[i].x << "  Y: " << PCs[i].y << std::endl;
		}
	}
}

bool BoardGenerator::isInHigherBlockChanceZone(int x, int y) const
{
	//Oblicza miejsca z wi�kszym prawdopodobie�stwem wyst�pienia wirtyalnych przeszk�d
	//Aby skomplikowa� drog� z zasilacza do komputera

	for (int i = 0; i < PCs.size(); i++)
	{
		int middleX = (PCs[i].x + PSU.x) / 2, middleY = (PCs[i].y + PSU.y) / 2;
		if (sqrt(pow((middleX - x), 2) + pow((middleY - y), 2)) < canBePlaced.size() / 8) return true;
	}

	if (sqrt(pow(PSU.x / 2, 2) + pow(PSU.y / 2, 2)) < canBePlaced.size() / 8) return true;
	if (sqrt(pow((PSU.x + canBePlaced[0].size()) / 2, 2) + pow(PSU.y / 2, 2)) < canBePlaced.size() / 8) return true;
	if (sqrt(pow(PSU.x / 2, 2) + pow((PSU.y + canBePlaced.size()) / 2, 2)) < canBePlaced.size() / 8) return true;
	if (sqrt(pow((PSU.x + canBePlaced[0].size()) / 2, 2) + pow((PSU.y + canBePlaced.size()) / 2, 2)) < canBePlaced.size() / 8) return true;

	return false;
}

bool BoardGenerator::coordinateCorrectChecher(int x, int y) const
{
	if (x < 0 || x >= canBePlaced[0].size()) return false;
	if (y < 0 || y >= canBePlaced.size()) return false;
	return true;
}

bool BoardGenerator::hasBeenActivated(std::vector <rewindData>& rewindVector, int x, int y) const
{
	//Sprawdza, czy podana warto�� istnieje ju� w tablicy
	for (int i = 0; i < rewindVector.size(); i++)
	{
		if (rewindVector[i].x == x && rewindVector[i].y == y) return true;
	}
	return false;
}

bool BoardGenerator::hasBeenPlaced(std::vector <coordinateData>& rewindVector, int x, int y) const
{
	//Sprawdza, czy podana warto�� istnieje ju� w tablicy
	//<Dla innego systemu danych>
	for (int i = 0; i < rewindVector.size(); i++)
	{
		if (rewindVector[i].x == x && rewindVector[i].y == y) return true;
	}
	return false;
}

bool BoardGenerator::arePCsOnList(std::vector <rewindData>& rewindVector) const
{
	//Sprawdza, czy wszystkie komputery s� aktywne
	for (int j = 0; j < PCs.size(); j++)
	{
		bool any = false;
		for (int i = 0; i < rewindVector.size(); i++)
		{
			if (rewindVector[i].x - PCs[j].x  == 1 && rewindVector[i].y == PCs[j].y)
			{
				any = true;
				break;
			}
			else if (rewindVector[i].x - PCs[j].x == -1 && rewindVector[i].y == PCs[j].y)
			{
				any = true;
				break;
			}
			else if (rewindVector[i].x == PCs[j].x && rewindVector[i].y - PCs[j].y == 1)
			{
				any = true;
				break;
			}
			else if (rewindVector[i].x == PCs[j].x && rewindVector[i].y - PCs[j].y == -1)
			{
				any = true;
				break;
			}
		}
		if (!any) return false;
	}
	return true;
}

void BoardGenerator::obstacleCreator()
{
	//Tworzy wirtualne przeszkody na mapie
	bool PSUtest = false;
	bool PCStest = false;
	bool canPass = false;
	std::vector <bool> PCStestSequence;

	while (!canPass)
	{
		for (int y = 0; y < canBePlaced.size(); y++)
			for (int x = 0; x < canBePlaced[0].size(); x++)
				canBePlaced[y][x] = true;

		for (int i = 0; i < PCs.size(); i++)
		{
			canBePlaced[PCs[i].y][PCs[i].x] = false;
		}


		PCStestSequence.clear();
		for (int y = 0; y < canBePlaced.size(); y++)
		{
			for (int x = 0; x < canBePlaced[0].size(); x++)
			{
				if (isInHigherBlockChanceZone(x, y))
				{
					if (rand() % 10 < 4) canBePlaced[y][x] = false;
				}
				else if (rand() % 10 < 2) canBePlaced[y][x] = false;
			}
		}

		//Sprawdzenie dost�pno�ci do zasilacza i PC
		//Nie mo�e blokowa� zasilacza, ani PC
		bool PSUtest = false;
		bool PCStest = false;

		if (coordinateCorrectChecher(PSU.x, PSU.y - 1))
		{
			if (canBePlaced[PSU.y - 1][PSU.x]) PSUtest = true;
		}
		else if (coordinateCorrectChecher(PSU.x + 1, PSU.y))
		{
			if (canBePlaced[PSU.y][PSU.x + 1]) PSUtest = true;
		}
		else if (coordinateCorrectChecher(PSU.x, PSU.y + 1))
		{
			if (canBePlaced[PSU.y + 1][PSU.x]) PSUtest = true;
		}
		else if (coordinateCorrectChecher(PSU.x - 1, PSU.y))
		{
			if (canBePlaced[PSU.y][PSU.x - 1]) PSUtest = true;
		}


		for (int i = 0; i < PCs.size(); i++)
		{
			if (coordinateCorrectChecher(PCs[i].x, PCs[i].y - 1))
			{
				if (canBePlaced[PCs[i].y - 1][PCs[i].x]) PCStestSequence.push_back(true);
			}
			else if (coordinateCorrectChecher(PCs[i].x + 1, PCs[i].y))
			{
				if (canBePlaced[PCs[i].y][PCs[i].x + 1]) PCStestSequence.push_back(true);
			}
			else if (coordinateCorrectChecher(PCs[i].x, PCs[i].y + 1))
			{
				if (canBePlaced[PCs[i].y + 1][PCs[i].x]) PCStestSequence.push_back(true);
			}
			else if (coordinateCorrectChecher(PCs[i].x - 1, PCs[i].y))
			{
				if (canBePlaced[PCs[i].y][PCs[i].x - 1]) PCStestSequence.push_back(true);
			}
			else PCStestSequence.push_back(false);
		}

		PCStest = true;
		for (int i = 0; i < PCStestSequence.size(); i++)
		{
			if (!PCStestSequence[i])
			{
				PCStest = false;
				break;
			}
		}
		if (PCStest && PSUtest) canPass = true;
	}


	if (debugMode)
	{
		std::cout << std::endl;

		std::cout << "  ";
		for (int x0 = 0; x0 < map[0].size(); x0++)
			std::cout << std::hex << x0 << " ";

		std::cout << std::endl;
		for (int y = 0; y < canBePlaced.size(); y++)
		{
			std::cout << std::hex << y << " ";
			for (int x = 0; x < canBePlaced[0].size(); x++)
			{
				bool isPC = false;
				for (int i = 0; i < PCs.size(); i++)
				{
					if ((PCs[i].x == x) && (PCs[i].y == y))
					{
						std::cout << "P ";
						isPC = true;
					}
				}
				if ((PSU.x == x) && (PSU.y == y))
				{
					std::cout << "Q ";
					isPC = true;
				}

				if (!isPC)
				{
					if (canBePlaced[y][x])
						std::cout << "# ";
					else std::cout << "  ";
				}
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
}

void BoardGenerator::onListAdder(std::vector <rewindData>& rewindVector, int x, int y, int p)
{
	//Je�eli blok spe�nia odpowiednie warunki, jest dodawany do listy
	if (coordinateCorrectChecher(x, y))
	{
		if (!hasBeenActivated(rewindVector, x, y))
		{
			if (canBePlaced[y][x])
			{
				rewindData zero;
				zero.previousID = p;
				zero.x = x;
				zero.y = y;
				rewindVector.push_back(zero);
			}
		}
	}
}

int BoardGenerator::PCCoordinateID(std::vector <rewindData>& rewindVector, int x, int y)
{
	//Zwraca ID bloku
	for (int i = 0; i < rewindVector.size(); i++)
	{
		if (rewindVector[i].x - x == 1 && rewindVector[i].y == y) return i;
		if (rewindVector[i].x - x == -1 && rewindVector[i].y == y) return i;
		if (rewindVector[i].x == x && rewindVector[i].y - y == 1) return i;
		if (rewindVector[i].x == x && rewindVector[i].y - y == -1) return i;
	}
	return -1;
}

bool BoardGenerator::pathFinder()
{
	//Sprawdza, czy od zasilacza da si� doj�� do komputer�w
	generatedPaths.clear();
	std::vector <rewindData> base;
	int powt = 0;
	rewindData zero;
	zero.previousID = -1;
	zero.x = PSU.x;
	zero.y = PSU.y;
	base.push_back(zero);

	while (powt < base.size())
	{
		int tempx = base[powt].x;
		int tempy = base[powt].y;
		onListAdder(base, tempx, tempy - 1, powt);
		onListAdder(base, tempx + 1, tempy, powt);
		onListAdder(base, tempx, tempy + 1, powt);
		onListAdder(base, tempx - 1, tempy, powt);
		powt++;
	}

	if (!arePCsOnList(base)) return false;//Je�eli nie znaleziono drogi do wsztskich komputer�w, zwraca fa�sz

	if (debugMode)
	{
		for (int i = 0; i < base.size(); i++)
		{
			std::cout << std::dec << "id: " << i << "  X: " << base[i].x << "  Y: " << base[i].y << "  P: " << base[i].previousID << std::endl;
		}
	}

	for (int i = 0; i < PCs.size(); i++)//Tworzenie �cie�ki 
	{
		int id = PCCoordinateID(base, PCs[i].x, PCs[i].y);
		while (id != -1)
		{
			coordinateData pathPiece;
			pathPiece.x = base[id].x;
			pathPiece.y = base[id].y;

			if (!hasBeenPlaced(generatedPaths, pathPiece.x, pathPiece.y))
			{
				generatedPaths.push_back(pathPiece);
			}

			id = base[id].previousID;
		}
	}
	return true;
}

void BoardGenerator::mapCreator()
{
	//Tworzy �cie�ki (wype�nia je odpowiednimi blokami)
	if (debugMode) std::cout << std::endl;

	for (int i = 0; i < generatedPaths.size(); i++)
	{
		if(debugMode) std::cout << "x: " << generatedPaths[i].x << "  y: " << generatedPaths[i].y << std::endl;


		bool surrounding[4] = { false,false,false,false };
		//   0
		// 3   1
		//   2

		for (int j = 0; j < generatedPaths.size(); j++)
		{
			if (i != j)
			{
				if (generatedPaths[i].x == generatedPaths[j].x)
					if (generatedPaths[i].y - generatedPaths[j].y == 1)
						surrounding[0] = true;

				if (generatedPaths[i].x == generatedPaths[j].x)
					if (generatedPaths[i].y - generatedPaths[j].y == -1)
						surrounding[2] = true;

				if (generatedPaths[i].y == generatedPaths[j].y)
					if (generatedPaths[i].x - generatedPaths[j].x == 1)
						surrounding[3] = true;

				if (generatedPaths[i].y == generatedPaths[j].y)
					if (generatedPaths[i].x - generatedPaths[j].x == -1)
						surrounding[1] = true;
			}
		}

		for (int w = 0; w < PCs.size(); w++)
		{
			if (generatedPaths[i].x == PCs[w].x)
			{
				if (generatedPaths[i].y - PCs[w].y == 1)
					surrounding[0] = true;
			}
			if (generatedPaths[i].x == PCs[w].x)
			{
				if (generatedPaths[i].y - PCs[w].y == -1)
					surrounding[2] = true;
			}
			if (generatedPaths[i].y == PCs[w].y)
			{
				if (generatedPaths[i].x - PCs[w].x == 1)
					surrounding[3] = true;
					
			}
			if (generatedPaths[i].y == PCs[w].y)
			{
				if (generatedPaths[i].x - PCs[w].x == -1)
					surrounding[1] = true;
			}
		}

		int suma = 0;
		for (int e = 0; e < 4; e++) if (surrounding[e]) suma++;


		if (surrounding[0] && surrounding[1] && surrounding[2] && surrounding[3])
			map[generatedPaths[i].y][generatedPaths[i].x].type = X;

		else if (suma == 3) map[generatedPaths[i].y][generatedPaths[i].x].type = E;

		else if ((surrounding[0] && surrounding[2]) || (surrounding[1] && surrounding[3]))
		{
			if (rand() % 2) map[generatedPaths[i].y][generatedPaths[i].x].type = I;
			else map[generatedPaths[i].y][generatedPaths[i].x].type = C;
		}

		else if (rand() % 2) map[generatedPaths[i].y][generatedPaths[i].x].type = L;
		else map[generatedPaths[i].y][generatedPaths[i].x].type = H;
	}


	if (debugMode)
	{
		std::cout << "  ";
		for (int x0 = 0; x0 < map[0].size(); x0++)
			std::cout << std::hex << x0 << " ";

		std::cout << std::endl;

		for (int y = 0; y < map.size(); y++)
		{
			std::cout << std::hex << y << " ";

			for (int x = 0; x < map[0].size(); x++)
			{
				bool czybyl = false;
				for (int i = 0; i < generatedPaths.size(); i++)
				{
					if (x == generatedPaths[i].x && y == generatedPaths[i].y)
					{
						std::cout << "# ";
						czybyl = true;
						break;
					}
				}
				if (!czybyl) std::cout << "  ";
			}
			std::cout << std::endl;
		}
	}

}

void BoardGenerator::nullFiller()
{
	//Wype�nia puste pola
	for (int y = 0; y < map.size(); y++)
	{
		for (int x = 0; x < map[0].size(); x++)
		{
			if (map[y][x].type == NONE)
			{
				int nr = rand() % 11;
				if (nr == 0) map[y][x].type = I;
				if (nr >= 1 && nr <= 3) map[y][x].type = L;
				if (nr == 4) map[y][x].type = E;
				if (nr == 5) map[y][x].type = X;
				if (nr >= 6 && nr <= 7) map[y][x].type = H;
				if (nr == 8) map[y][x].type = C;
				if (nr >= 9) map[y][x].type = NONE;
			}

			int rotrand = rand() % 4;

			if (rotrand == 1) map[y][x].rot = RIGHT;
			if (rotrand == 2) map[y][x].rot = DOWN;
			if (rotrand == 3) map[y][x].rot = LEFT;
		}
	}

	for (int i = 0; i < PCs.size(); i++)
	{
		map[PCs[i].y][PCs[i].x].type = NONE;
	}
}

void BoardGenerator::useDebugMode()
{
	debugMode = true;
}

void BoardGenerator::exitDebugMode()
{
	debugMode = false;
}

void BoardGenerator::generateMap(int PCAmount)
{
	//G��wny "sterownik" tworzenia
	//Wywo�uje wszystkie procesy po kolei

	recreateBoolMap();

	//Losowanie po�o�enia zasilacza
	PSU.x = rand() % (map[0].size());
	PSU.y = rand() % (map.size());

	//Losowanie po�o�enia komputer�w
	placePCs(PCAmount);

	//Tworzy wirtualne przeszkody, aby skomplikowa� po��czenia komputer-zasilacz
	obstacleCreator();

	//Dop�ki nie da si� po��czy� zasilacza i komputera, losuje nowy uk��d przeszk�d
	while (!pathFinder())
	{
		obstacleCreator();
	}

	//Bie�e pod uwag� wirtualne przeszkody i uk�ada �cie�ki ��cz�ce komputery i zasilacz
	mapCreator();

	//Wype�nia puste pola (pozosta�e po generowaniu �cierzek)
	//Losow� zawarto�ci�
	nullFiller();

	//Zapisanie pozycji komp�ter�w i zasilacza na mapie
	for (int i = 0; i < PCs.size(); i++)
		map[PCs[i].y][PCs[i].x].isPC = true;
	map[PSU.y][PSU.x].isPSU = true;

	if (debugMode) std::cout << "[Jestem message'm] Mapa wygenerowana" << std::endl;
}