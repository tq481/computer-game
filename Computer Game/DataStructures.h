#pragma once

//Wszystkie enumy i struktury w jednym miejscu

enum poolType { I, L, E, X, H, C, NONE };
enum rotation { UP, RIGHT, DOWN, LEFT };
enum whereIAm { MAIN, INFO, PRE, GAME, END };


struct Pixel
{
	bool isPC;
	bool isPSU;
	bool isActive0;
	bool isActive1;
	poolType type;
	rotation rot;
};

struct rewindData
{
	int x;
	int y;
	int previousID;
};

struct coordinateData
{
	int x;
	int y;
};

struct mapBools
{
	bool up;
	bool right;
	bool down;
	bool left;
};

struct PreGameData
{
	int width;
	int height;
	int PCs;
};