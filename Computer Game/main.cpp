//Project 3 Object Programming
//2020-2021 copyright Jakub Olszewski 253025

#include <iostream>
#include <random>
#include <ctime>
#include <cmath>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include "DataStructures.h"
#include "BoardGenerator.h"
#include "BoardController.h"
#include "graphicsEngine.h"
#include "inputHandler.h"


int main()
{
	sf::RenderWindow outputWindow(sf::VideoMode(1400, 1000), "PCC");
	srand(time(NULL));
	std::vector< std::vector <Pixel> > map;
	graphicsEngine engine(outputWindow, map);
	BoardGenerator generator(map);
	BoardController controller(map, generator);
	inputHandler chief(outputWindow, engine, controller);

	while (outputWindow.isOpen())
	{
		sf::Event event;
		while (outputWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				outputWindow.close();
			if(event.type == sf::Event::MouseButtonPressed)
				chief.passEvent(event);
		}
		if (!chief.switcher())
			outputWindow.close();
		outputWindow.display();
	}
}