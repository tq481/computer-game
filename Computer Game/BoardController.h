﻿#pragma once

class BoardController
{
	//Klasa odpowiedzialna za kontrolowanie mapy
	std::vector< std::vector <Pixel> >& map;
	BoardGenerator& generator;
	int width, height, PCs;
	bool coordinateCorrectChecher(int x, int y);
	bool poolActivityChecker2(int input, int x, int y);
	void clearPaths();
	void activation(int x, int y, int xp, int yp);
	void pathFinderAddition(std::vector <rewindData>& regresFollower, int x, int y);
	void ALBAdder(std::vector <rewindData> &rw, int x, int y, int entrance);
	bool ALBChecker(std::vector <rewindData> &rw, int x, int y, int entrance);
	std::vector< std::vector <mapBools> > activicyPath;
	int PSUX, PSUY;
public:
	BoardController(std::vector< std::vector <Pixel> >& m, BoardGenerator& g);
	void initiateBoard(int width, int height, int PCs);
	bool mapUpdater(int x, int y, bool rotation);
	void debugDisplay();
	bool connectionChecker();
};

BoardController::BoardController(std::vector< std::vector <Pixel> >& m, BoardGenerator& g) : map(m), generator(g) {}

void BoardController::initiateBoard(int width, int height, int PCs)
{
	//Tworzy wektor wektorów jako mapę, na której będą przechowywane wartości logiczne
	//Argumenty:
	//szerokość mapy
	//wysokość mapy
	//ilość komputerów

	map.clear();
	activicyPath.clear();
	Pixel defaultPixel;
	defaultPixel.isActive0 = false; //Czy płynie prąd
	defaultPixel.isActive1 = false; //Czy płynie prąd (w ypadku złożonych bloków)
	defaultPixel.isPC = false; //Czy jest komputerem
	defaultPixel.isPSU = false; //Czy jest zasilaczem
	defaultPixel.rot = UP; //W którą stronę jest obrócony
	defaultPixel.type = NONE; //Typ bloku

	//Rotacje: UP, RIGHT, DOW< LEFT
	//Typy: I, L, E, X, H, C, NONE
	//NONE to pusty blok, lub zasilacz, albo PC

	//mapBools wykorzystywane jest do obliczania połączeń między blokami
	mapBools defaultPixelBool;
	defaultPixelBool.up = false;
	defaultPixelBool.right = false;
	defaultPixelBool.down = false;
	defaultPixelBool.left = false;

	for (int y = 0; y < height; y++)
	{
		std::vector <Pixel> pom;
		std::vector < mapBools> pom1;
		for (int x = 0; x < width; x++)
		{
			pom.push_back(defaultPixel);
			pom1.push_back(defaultPixelBool);
		}
		map.push_back(pom);
		activicyPath.push_back(pom1);
	}
	BoardController::width = width;
	BoardController::height = height;
	BoardController::PCs = PCs;

	generator.generateMap(PCs);

	for (int y = 0; y < map.size(); y++)
	{
		for (int x = 0; x < map[0].size(); x++)
		{
			if (map[y][x].isPSU)
			{
				PSUX = x;
				PSUY = y;
				break;
			}
		}
	}
}

bool BoardController::coordinateCorrectChecher(int x, int y)
{
	//Sprawdzenie czy podane koordynaty mieszczą się w obrębie mapy
	if (x < 0 || x >= width) return false;
	if (y < 0 || y >= height) return false;
	return true;
}

bool BoardController::mapUpdater(int x, int y, bool rotation)
{
	//Służy do obracania bloków
	//Po aktualizacji inicjuje przeliczenie połączeń
	//Argumenty:
	//X bloku
	//Y bloku
	//rortacja (true = zgodnie z ruchem wskazówek zegara)

	if (!coordinateCorrectChecher(x, y)) return false;

	if (map[y][x].type == NONE) return false;
	if (map[y][x].isPC) return false;
	if (map[y][x].isPSU) return false;
	if (rotation)
	{
		if (map[y][x].rot == UP) map[y][x].rot = RIGHT;
		else if (map[y][x].rot == RIGHT) map[y][x].rot = DOWN;
		else if (map[y][x].rot == DOWN) map[y][x].rot = LEFT;
		else if (map[y][x].rot == LEFT) map[y][x].rot = UP;
	}
	else
	{
		if (map[y][x].rot == UP) map[y][x].rot = LEFT;
		else if (map[y][x].rot == LEFT) map[y][x].rot = DOWN;
		else if (map[y][x].rot == DOWN) map[y][x].rot = RIGHT;
		else if (map[y][x].rot == RIGHT) map[y][x].rot = UP;
	}


	return connectionChecker(); //if true => Game Won!
}

bool BoardController::poolActivityChecker2(int input, int x, int y)
{
	//Tworzy połączenia między blokami na podstawie modeli typów bloków
	//oraz ich rotacji


	if (map[y][x].isPC) return true;
	if (map[y][x].type == NONE) return false;
	

	if (map[y][x].isPSU)
	{
		activicyPath[y][x].up = true;
		activicyPath[y][x].right = true;
		activicyPath[y][x].left = true;
		activicyPath[y][x].down = true;
		return true;
	}

	if (map[y][x].type == C)
	{
		if (input == 0)
		{
			activicyPath[y][x].down = true;
			return true;
		}
		if (input == 1)
		{
			activicyPath[y][x].left = true;
			return true;
		}
		if (input == 2)
		{
			activicyPath[y][x].up = true;
			return true;
		}
		if (input == 3)
		{
			activicyPath[y][x].right = true;
			return true;
		}
	}

	if (input == 0)
	{
		if (map[y][x].type == I)
		{
			if (map[y][x].rot == UP || map[y][x].rot == DOWN)
			{
				activicyPath[y][x].down = true;
				return true;
			}
		}
		else if (map[y][x].type == L)
		{
			if (map[y][x].rot == UP)
			{
				activicyPath[y][x].right = true;
				return true;
			}
			if (map[y][x].rot == LEFT)
			{
				activicyPath[y][x].left = true;
				return true;
			}
		}
		else if (map[y][x].type == E)
		{
			if (map[y][x].rot == UP)
			{
				activicyPath[y][x].right = true;
				activicyPath[y][x].down = true;
				return true;
			}
			if (map[y][x].rot == DOWN)
			{
				activicyPath[y][x].left = true;
				activicyPath[y][x].down = true;
				return true;
			}
			if (map[y][x].rot == LEFT)
			{
				activicyPath[y][x].left = true;
				activicyPath[y][x].right = true;
				return true;
			}
		}
		else if (map[y][x].type == H)
		{
			if (map[y][x].rot == UP || map[y][x].rot == DOWN)
			{
				activicyPath[y][x].right = true;
				return true;
			}
			if (map[y][x].rot == LEFT || map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].left = true;
				return true;
			}
		}
		else if (map[y][x].type == X)
		{
			activicyPath[y][x].right = true;
			activicyPath[y][x].down = true;
			activicyPath[y][x].left = true;
			return true;
		}
	}

	if (input == 1)
	{
		if (map[y][x].type == I)
		{
			if (map[y][x].rot == LEFT || map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].left = true;
				return true;
			}
		}
		else if (map[y][x].type == L)
		{
			if (map[y][x].rot == UP)
			{
				activicyPath[y][x].up = true;
				return true;
			}
			if (map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].down = true;
				return true;
			}
		}
		else if (map[y][x].type == E)
		{
			if (map[y][x].rot == UP)
			{
				activicyPath[y][x].up = true;
				activicyPath[y][x].down = true;
				return true;
			}
			if (map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].left = true;
				activicyPath[y][x].down = true;
				return true;
			}
			if (map[y][x].rot == LEFT)
			{
				activicyPath[y][x].up = true;
				activicyPath[y][x].left = true;
				return true;
			}
		}
		else if (map[y][x].type == H)
		{
			if (map[y][x].rot == UP || map[y][x].rot == DOWN)
			{
				activicyPath[y][x].up = true;
				return true;
			}
			if (map[y][x].rot == LEFT || map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].down = true;
				return true;
			}
		}
		else if (map[y][x].type == X)
		{
			activicyPath[y][x].up = true;
			activicyPath[y][x].down = true;
			activicyPath[y][x].left = true;
			return true;
		}
	}

	if (input == 2)
	{
		if (map[y][x].type == I)
		{
			if (map[y][x].rot == UP || map[y][x].rot == DOWN)
			{
				activicyPath[y][x].up = true;
				return true;
			}
		}
		else if (map[y][x].type == L)
		{
			if (map[y][x].rot == DOWN)
			{
				activicyPath[y][x].left = true;
				return true;
			}
			if (map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].right = true;
				return true;
			}
		}
		else if (map[y][x].type == E)
		{
			if (map[y][x].rot == UP)
			{
				activicyPath[y][x].up = true;
				activicyPath[y][x].right = true;
				return true;
			}
			if (map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].right = true;
				activicyPath[y][x].left = true;
				return true;
			}
			if (map[y][x].rot == DOWN)
			{
				activicyPath[y][x].up = true;
				activicyPath[y][x].left = true;
				return true;
			}
		}
		else if (map[y][x].type == H)
		{
			if (map[y][x].rot == UP || map[y][x].rot == DOWN)
			{
				activicyPath[y][x].left = true;
				return true;
			}
			if (map[y][x].rot == LEFT || map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].right = true;
				return true;
			}
		}
		else if (map[y][x].type == X)
		{
			activicyPath[y][x].up = true;
			activicyPath[y][x].right = true;
			activicyPath[y][x].left = true;
			return true;
		}
	}

	if (input == 3)
	{
		if (map[y][x].type == I)
		{
			if (map[y][x].rot == LEFT || map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].right = true;
				return true;
			}
		}
		else if (map[y][x].type == L)
		{
			if (map[y][x].rot == LEFT)
			{
				activicyPath[y][x].up = true;
				return true;
			}
			if (map[y][x].rot == DOWN)
			{
				activicyPath[y][x].down = true;
				return true;
			}
		}
		else if (map[y][x].type == E)
		{
			if (map[y][x].rot == DOWN)
			{
				activicyPath[y][x].up = true;
				activicyPath[y][x].down = true;
				return true;
			}
			if (map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].right = true;
				activicyPath[y][x].down = true;
				return true;
			}
			if (map[y][x].rot == LEFT)
			{
				activicyPath[y][x].up = true;
				activicyPath[y][x].right = true;
				return true;
			}
		}
		else if (map[y][x].type == H)
		{
			if (map[y][x].rot == UP || map[y][x].rot == DOWN)
			{
				activicyPath[y][x].down = true;
				return true;
			}
			if (map[y][x].rot == LEFT || map[y][x].rot == RIGHT)
			{
				activicyPath[y][x].up = true;
				return true;
			}
		}
		else if (map[y][x].type == X)
		{
			activicyPath[y][x].up = true;
			activicyPath[y][x].right = true;
			activicyPath[y][x].down = true;
			return true;
		}
	}
	return false;
}

void BoardController::clearPaths()
{
	//Czyści wartości logiczne tablic
	//Przygotowywuje do ponownego obliczania połączeń

	for (int y = 0; y < activicyPath.size(); y++)
	{
		for (int x = 0; x < activicyPath[y].size(); x++)
		{
			activicyPath[y][x].up = false;
			activicyPath[y][x].right = false;
			activicyPath[y][x].down = false;
			activicyPath[y][x].left = false;

			map[y][x].isActive0 = false;
			map[y][x].isActive1 = false;
		}
	}
}

void BoardController::pathFinderAddition(std::vector <rewindData>& regresFollower, int x, int y)
{
	//Dodaje nowe połączenia do listy

	rewindData newCoordinate;
	newCoordinate.x = x;
	newCoordinate.y = y;

	int id = -1;
	for (int i = 0; i < regresFollower.size(); i++)
	{
		if (regresFollower[i].x == x && regresFollower[i].y == y)
		{
			id = i;
			break;
		}
	}
	newCoordinate.previousID = id;
	
	regresFollower.push_back(newCoordinate);

}

void BoardController::ALBAdder(std::vector <rewindData> &rw, int x, int y, int entrance)
{
	//Anti Loop Back zapobiega "krążeniu" sygnału kilkakrotnie przez te same ścieżki
	//ALBAdder dodaje aktywowane ścieżki do wykluczenia

	rewindData ALB;
	ALB.x = x;
	ALB.y = y;
	ALB.previousID = entrance;

	rw.push_back(ALB);
}

bool BoardController::ALBChecker(std::vector <rewindData> &rw, int x, int y, int entrance)
{
	//ALBChecker sprawdza, czy dana ścieżka była już aktywowana

	for (int i = 0; i < rw.size(); i++)
	{
		if (rw[i].x == x && rw[i].y == y && rw[i].previousID == entrance) return true;
	}
	return false;
}

bool BoardController::connectionChecker()
{
	//Funkcja sprawdzająca połączenia między blokami

	clearPaths();
	int end = 0;

	std::vector <rewindData> regresFollower;
	std::vector <rewindData> antiLoopBack;

	rewindData temp;
	temp.x = PSUX;
	temp.y = PSUY;
	temp.previousID = -1;
	regresFollower.push_back(temp);

	activicyPath[temp.y][temp.x].up = true;
	activicyPath[temp.y][temp.x].right = true;
	activicyPath[temp.y][temp.x].down = true;
	activicyPath[temp.y][temp.x].left = true;

	int licznik = 0;

	while (licznik < regresFollower.size())
	{
		int tempx = regresFollower[licznik].x;
		int tempy = regresFollower[licznik].y;

		if (activicyPath[tempy][tempx].up)//Jażeli da się aktywować blok <w tym wypadku wyżej>
		{
			if (coordinateCorrectChecher(tempx, tempy - 1))//Jeżeli ma poprawne koordynaty
			{
				if (poolActivityChecker2(2, tempx, tempy - 1))//Jeżeli blok <wyżej> może przyjąć sygnał
				{
					if (!ALBChecker(antiLoopBack, tempx, tempy - 1, 2))//Jeżeli przez daną ścieżkę nie przechodził jeszcze żaden sygnał
					{
						pathFinderAddition(regresFollower, tempx, tempy - 1);//Dodać blok do listy sprawdzanych
						activation(tempx, tempy - 1, tempx, tempy);//Aktywować blok
						ALBAdder(antiLoopBack, tempx, tempy - 1, 2);//Anti Loop Back
					}
				}

			}
		}
		if (activicyPath[tempy][tempx].right)//To samo co wyżej, tylko dla bloku po <prawej> stronie
		{
			if (coordinateCorrectChecher(tempx + 1, tempy))
			{
				if (poolActivityChecker2(3, tempx + 1, tempy))
				{
					if (!ALBChecker(antiLoopBack, tempx + 1, tempy, 3))
					{
						pathFinderAddition(regresFollower, tempx + 1, tempy);
						activation(tempx + 1, tempy, tempx, tempy);
						ALBAdder(antiLoopBack, tempx + 1, tempy, 3);
					}
				}

			}
		}
		if (activicyPath[tempy][tempx].down)
		{
			if (coordinateCorrectChecher(tempx, tempy + 1))
			{
				if (poolActivityChecker2(0, tempx, tempy + 1))
				{
					if (!ALBChecker(antiLoopBack, tempx, tempy + 1, 0))
					{
						pathFinderAddition(regresFollower, tempx, tempy + 1);
						activation(tempx, tempy + 1, tempx, tempy);
						ALBAdder(antiLoopBack, tempx, tempy + 1, 0);
					}
				}

			}
		}
		if (activicyPath[tempy][tempx].left)
		{
			if (coordinateCorrectChecher(tempx - 1, tempy))
			{
				if (!ALBChecker(antiLoopBack, tempx - 1, tempy, 1))
				{
					if (poolActivityChecker2(1, tempx - 1, tempy))
					{
						pathFinderAddition(regresFollower, tempx - 1, tempy);
						activation(tempx - 1, tempy, tempx, tempy);
						ALBAdder(antiLoopBack, tempx - 1, tempy, 1);
					}
				}

			}
		}
		licznik++;
	}

	//Sprawdzenie, czy wszystkie komputery zostały aktywowane
	for (int y = 0; y < map.size(); y++)
		for (int x = 0; x < map[0].size(); x++)
			if (map[y][x].isPC && map[y][x].isActive0)end++;


	return (end == PCs);
}

void BoardController::activation(int x, int y, int xp, int yp)
{
	//Aktywowanie ścieżki w danym bloku

	if (map[y][x].isPC) map[y][x].isActive0 = true;

	if (map[y][x].type == C)
	{
		if (x == xp)
			map[y][x].isActive0 = true;
		else map[y][x].isActive1 = true;
	}

	else if (map[y][x].type == H)
	{
		if (map[y][x].rot == UP || map[y][x].rot == DOWN)
		{
			if (x == xp)
			{
				if (yp > y)
					map[y][x].isActive1 = true;
				else map[y][x].isActive0 = true;
			}
			else
			{
				if (xp > x)
					map[y][x].isActive0 = true;
				else map[y][x].isActive1 = true;
			}
		}
		if (map[y][x].rot == LEFT || map[y][x].rot == RIGHT)
		{
			if (x == xp)
			{
				if (yp > y)
					map[y][x].isActive1 = true;
				else map[y][x].isActive0 = true;
			}
			else
			{
				if (xp > x)
					map[y][x].isActive1 = true;
				else map[y][x].isActive0 = true;
			}
		}
	}

	else map[y][x].isActive0 = true;
}

void BoardController::debugDisplay()
{
	//Wyświetlanie danych do debugowania
	std::cout << std::endl;

	if (map.size() == 0)
	{
		std::cout << "[Jestem Errorem] Mapa nie zostala wygenerowana; nic do pokazania" << std::endl;
		return;
	}

	std::cout << "  ";
	for (int x0 = 0; x0 < map[0].size(); x0++)
		std::cout << std::hex << x0 << " ";

	std::cout << std::endl;

	for (int y = 0; y < map.size(); y++)
	{
		std::cout << std::hex << y << " ";
		for (int x = 0; x < map[0].size(); x++)
		{
			if (map[y][x].isPC) std::cout << "P ";
			else if (map[y][x].isPSU) std::cout << "Q ";
			else if (map[y][x].type == I) std::cout << "I ";
			else if (map[y][x].type == L) std::cout << "L ";
			else if (map[y][x].type == E) std::cout << "E ";
			else if (map[y][x].type == X) std::cout << "X ";
			else if (map[y][x].type == H) std::cout << "H ";
			else if (map[y][x].type == C) std::cout << "C ";
			else if (map[y][x].type == NONE) std::cout << "_ ";
		}
		std::cout << std::endl;
	}
}